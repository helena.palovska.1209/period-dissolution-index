# Period Dissolution Index

**Intervals indexing** (ordered domain, e.g. datetime or numbers). **Search algorithm for point-looup of *O*(log(n)) efficiency.**

Interval is dissolved into set of maximal "base intervals" contained in it, index made upon it.

